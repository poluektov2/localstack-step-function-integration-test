import boto3
from typing import TypedDict, List
import os
from io import BytesIO
import json


class LambdaInputFields(TypedDict):
    document_bucket: str
    document_key: str


class LambdaOutputFields(TypedDict):
    split_document_bucket: str
    split_document_key: str


class LambdaEvent(LambdaInputFields, LambdaOutputFields):
    pass


def get_s3_client():
    if os.environ.get("ENV") == "LOCAL":
        return boto3.client('s3', aws_access_key_id='', aws_secret_access_key='', region_name='eu-west-2',
                            endpoint_url=f'http://{os.environ["LOCALSTACK_HOSTNAME"]}:4566')
    else:
        return boto3.client('s3')


def split_text(text: str) -> List[str]:
    return text.split(".")


def lambda_handler(event: LambdaEvent, context) -> LambdaEvent:
    s3_client = get_s3_client()

    # read the input file
    stream = BytesIO()
    s3_client.download_fileobj(
        event["document_bucket"], event["document_key"], stream)
    text = stream.getvalue().decode('UTF-8')

    # split into sentences
    sentences = split_text(text)
    byte_string = str.encode(json.dumps({'sentences': sentences}))

    # write into s3
    output_key = "split_{}".format(event["document_key"])
    s3_client.upload_fileobj(BytesIO(byte_string),
                             event["document_bucket"], output_key)

    return {
        "split_document_bucket": event["document_bucket"],
        "split_document_key": output_key
    }
