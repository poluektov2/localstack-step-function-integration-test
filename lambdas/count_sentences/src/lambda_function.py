import boto3
import os
import json
from io import BytesIO
from typing import TypedDict


class LambdaInputFields(TypedDict):
    split_document_bucket: str
    split_document_key: str


class LambdaOutputFields(TypedDict):
    n_sentences: int
    lang: str


class LambdaEvent(LambdaInputFields, LambdaOutputFields):
    pass


def get_s3_client():
    if os.environ.get("ENV") == "LOCAL":
        return boto3.client('s3', aws_access_key_id='', aws_secret_access_key='', region_name='eu-west-2',
                            endpoint_url=f'http://{os.environ["LOCALSTACK_HOSTNAME"]}:4566')
    else:
        return boto3.client('s3')


def lambda_handler(event: LambdaEvent, context) -> LambdaEvent:
    s3_client = get_s3_client()

    # read the input file
    stream = BytesIO()
    s3_client.download_fileobj(
        event["split_document_bucket"], event["split_document_key"], stream)
    sentences = json.loads(stream.getvalue().decode('UTF-8'))["sentences"]

    return {
        "n_sentences": len(sentences),
        "lang": "en"
    }
