import json
import os
import unittest

from localstack.utils.aws import aws_stack
from localstack.utils import testutil
from localstack.utils.common import load_file, retry
from pathlib import Path

ROOT_FOLDER = Path(__file__).parent.parent.absolute()
print(ROOT_FOLDER)


class TestSplitAndCount(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.lambda_client = aws_stack.connect_to_service("lambda")
        cls.s3_client = aws_stack.connect_to_service("s3")
        cls.sfn_client = aws_stack.connect_to_service("stepfunctions")

        split_documents_zip = testutil.create_lambda_archive(
            load_file(os.path.join(ROOT_FOLDER, "lambdas", "split_document", "src", "lambda_function.py")),
            file_name="lambda_function.py",
            get_content=True,
        )
        count_sentences_zip = testutil.create_lambda_archive(
            load_file(os.path.join(ROOT_FOLDER, "lambdas", "count_sentences", "src", "lambda_function.py")),
            file_name="lambda_function.py",
            get_content=True,
        )

        testutil.create_lambda_function(
            func_name="SplitDocument",
            zip_file=split_documents_zip,
            handler="lambda_function.lambda_handler",
            runtime="python3.8",
            envvars={"ENV": "LOCAL"},
        )
        testutil.create_lambda_function(
            func_name="CountSentences",
            zip_file=count_sentences_zip,
            handler="lambda_function.lambda_handler",
            runtime="python3.8",
            envvars={"ENV": "LOCAL"},
        )

        role_arn = aws_stack.role_arn("sfn_role")
        split_document_arn = aws_stack.lambda_function_arn("SplitDocument")
        count_sentences_arn = aws_stack.lambda_function_arn("CountSentences")

        with open(os.path.join(ROOT_FOLDER, "step_functions", "split_and_count.json")) as f:
            definition = json.load(f)

        definition["States"]["SplitDocument"]["Resource"] = split_document_arn
        definition["States"]["CountSentences"]["Resource"] = count_sentences_arn

        result = cls.sfn_client.create_state_machine(
            name="SplitAndCount", definition=json.dumps(definition), roleArn=role_arn
        )
        cls.sm_arn = result["stateMachineArn"]

        cls.s3_client.create_bucket(Bucket="documents")
        cls.s3_client.upload_file(os.path.join(ROOT_FOLDER, "tests", "test.txt"), Bucket="documents", Key="test.txt")

    @classmethod
    def tearDownClass(cls):

        # clean up lambdas
        cls.lambda_client.delete_function(FunctionName="SplitDocument")
        cls.lambda_client.delete_function(FunctionName="CountSentences")

        # clean up step function
        cls.sfn_client.delete_state_machine(stateMachineArn=cls.sm_arn)

        # clean up s3
        response = cls.s3_client.list_objects(Bucket="documents")
        for object in response["Contents"]:
            cls.s3_client.delete_object(Bucket="documents", Key=object["Key"])
        cls.s3_client.delete_bucket(Bucket="documents")

    def test_create_run_state_machine(self):
        # run state machine
        with open(os.path.join(ROOT_FOLDER, "tests", "test_event.json")) as f:
            test_event = json.load(f)
        invocation_result = self.sfn_client.start_execution(stateMachineArn=self.sm_arn, input=json.dumps(test_event))
        assert invocation_result.get("executionArn")

        def check_invocation():
            result = self._get_execution_results(self.sm_arn)
            self.assertEqual({"lang": "en", "n_sentences": 7}, result)

        retry(check_invocation, sleep=1, retries=10)

    def _get_execution_results(self, sm_arn):
        response = self.sfn_client.list_executions(stateMachineArn=sm_arn)
        executions = sorted(response["executions"], key=lambda x: x["startDate"])
        execution = executions[-1]
        result = self.sfn_client.get_execution_history(executionArn=execution["executionArn"])
        events = sorted(result["events"], key=lambda event: event["timestamp"])
        return json.loads(events[-1]["executionSucceededEventDetails"]["output"])
