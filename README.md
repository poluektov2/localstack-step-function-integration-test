### Run Localstack

```
docker-compose up
```

### Deploy *SplitDocument* Lambda function as .zip and invoke it


```
cd lambdas/split_document/

pip install --target ./package -r requirements.txt

cd package

zip -r ../my-deployment-package.zip .

cd ../src

zip -r ../my-deployment-package.zip lambda_function.py 

cd ..

aws --endpoint-url http://localhost:4566 lambda create-function --function-name SplitDocument --runtime python3.8 --zip-file fileb://my-deployment-package.zip --handler lambda_function.lambda_handler --role arn:aws:iam::000000000:role/lambda-ex --environment Variables={ENV=LOCAL}

aws --endpoint-url http://localhost:4566 s3api create-bucket --bucket documents

aws --endpoint-url http://localhost:4566 s3 cp tests/test.txt s3://documents/test.txt

aws --endpoint-url http://localhost:4566 lambda invoke --function-name SplitDocument --payload fileb://tests/test_event.json tests/test_output.json

```

For `CountSentences` do the same but change the function name

### Create and execute the step function

```
aws --endpoint-url http://localhost:4566 stepfunctions create-state-machine --name SplitAndCount --definition file://step_functions/split_and_count.json --role-arn arn:aws:iam::000000000000:role/stepfunction-ex

aws --endpoint-url http://localhost:4566 stepfunctions start-execution --state-machine arn:aws:states:us-east-1:000000000000:stateMachine:SplitAndCount --input "{\"document_bucket\": \"documents\",  \"document_key\": \"test.txt\"}"

aws --endpoint-url http://localhost:4566 stepfunctions describe-execution --execution-arn 

```

### Run the integration test

```
python -m unittest tests/test_split_and_count.py
```

### General information

ECR is available in the Pro version € 15 / month per user + 14 days free trial.  
SQS, SNS available in the Community version.

